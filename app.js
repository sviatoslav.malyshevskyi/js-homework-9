'use strict';

const tabLinks = document.querySelectorAll('.tab-title');
const tabContent = document.querySelectorAll(".tab-content");

tabLinks.forEach(function (el) {
    el.addEventListener("click", openTab);
});

function openTab(el) {
    const btnTarget = el.currentTarget;
    const character = btnTarget.dataset.character;

    tabContent.forEach(function (el) {
        el.classList.remove("active");
    });

    tabLinks.forEach(function (el) {
        el.classList.remove("active");
    });

    document.querySelector("#" + character).classList.add("active");
    btnTarget.classList.add("active");
}